# Changelog

All notable changes to [ngpr-prodige-visualiseur](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur) project will be documented in this file.

## [4.4.41](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.40...4.4.41) - 2025-02-25

## [4.4.40](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.39...4.4.40) - 2024-12-24

## [4.4.39](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.38...4.4.39) - 2024-11-07

## [4.4.38](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.37...4.4.38) - 2024-10-22

## [4.4.37](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.36...4.4.37) - 2024-09-18

## [4.4.36](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.35...4.4.36) - 2024-09-11

## [4.4.35](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.34...4.4.35) - 2024-08-23

## [4.4.34](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.33...4.4.34) - 2024-07-09

## [4.4.33](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.32...4.4.33) - 2024-07-09

## [4.4.32](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.31...4.4.32) - 2024-07-08

## [4.4.31](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.30...4.4.31) - 2024-07-03

## [4.4.30](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.29...4.4.30) - 2024-06-28

## [4.4.29](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.28...4.4.29) - 2024-06-28

## [4.4.28](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.27...4.4.28) - 2024-06-28

## [4.4.27](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.26...4.4.27) - 2024-06-28

## [4.4.26](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.25...4.4.26) - 2024-06-28

## [4.4.25](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.24...4.4.25) - 2024-05-14

## [4.4.24](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.23...4.4.24) - 2024-05-14

## [4.4.23](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.22...4.4.23) - 2024-05-13

## [4.4.22](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.21...4.4.22) - 2024-05-07

## [4.4.21](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.20...4.4.21) - 2024-02-16

### Fix

- buffer and query behavior

## [4.4.20](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.19...4.4.20) - 2023-12-13

## [4.4.19](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.18...4.4.19) - 2023-11-28

## [4.4.18](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.17...4.4.18) - 2023-11-28

## [4.4.17](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.16...4.4.17) - 2023-10-18

## [4.4.16](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.15...4.4.16) - 2023-10-17

## [4.4.15](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.14...4.4.15) - 2023-09-21

### Fix

- layer tree checkbox
- layer tree checkbox

## [4.4.14](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.13...4.4.14) - 2023-09-21

## [4.4.13](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.12...4.4.13) - 2023-09-04

## [4.4.12](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.11...4.4.12) - 2023-07-11

## [4.4.11](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.10...4.4.11) - 2023-06-20

### Fix

- legend visibility fixes

## [4.4.10](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.9...4.4.10) - 2023-06-19

## [4.4.9](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.8...4.4.9) - 2023-06-19

## [4.4.8](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.7...4.4.8) - 2023-06-16

### Changed

- manage edition for data with different srs from map

## [4.4.7](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.6...4.4.7) - 2023-06-13

## [4.4.6](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.5...4.4.6) - 2023-06-07

## [4.4.5](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.4...4.4.5) - 2023-05-11

## [4.4.4](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.3...4.4.4) - 2023-04-12

## [4.4.3](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.2...4.4.3) - 2023-04-11

## [4.4.2](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4.1...4.4.2) - 2023-04-06

## [4.4.1](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc9...4.4.1) - 2023-04-04

## [4.4-rc9](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc8...4.4-rc9) - 2022-12-12

## [4.4-rc8](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc7...4.4-rc8) - 2022-12-07

## [4.4-rc7](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc6...4.4-rc7) - 2022-12-05

## [4.4-rc6](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc5...4.4-rc6) - 2022-11-24

## [4.4-rc5](https://gitlab.adullact.net/prodige/ngpr-prodige-visualiseur/compare/4.4-rc4...4.4-rc5) - 2022-11-15

