# Module PRODIGE visualiseur

Module de visualisation cartographique de PRODIGE (technologie Angular) basé sur les fichiers de contextes de cartes produits par PRODIGE et sur les webservices OGC. 

### configuration

Modifier le fichier src/env.js

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- [Accès au visualiseur](https://carto.prodige.internal/1/limites_administratives.map).
