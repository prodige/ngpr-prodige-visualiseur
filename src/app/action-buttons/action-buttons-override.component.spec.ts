import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionButtonsOverrideComponent } from './action-buttons-override.component';

describe('ActionButtonsOverrideComponent', () => {
  let component: ActionButtonsOverrideComponent;
  let fixture: ComponentFixture<ActionButtonsOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionButtonsOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionButtonsOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
