import {
  Component,
  OnInit,
  ContentChild,
  HostListener,
  OnDestroy,
  Input,
  ViewChild,
} from "@angular/core";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import {
  VisualiseurCoreService,
  MapService,
  MapIdService,
  AlertConfirmPromptModalComponent,
  EditingToolService,
} from "@alkante/visualiseur-core";
import { Subscription } from "rxjs";
import { FeaturesInfo } from "@alkante/visualiseur-core/lib/services/editing-tool.service";
import { ExportFoncierModalComponent } from "../modals/export-foncier-modal/export-foncier-modal.component";

@Component({
  selector: "alk-action-buttons-override",
  templateUrl: "./action-buttons-override.component.html",
  styleUrls: ["./action-buttons-override.component.scss"],
})
export class ActionButtonsOverrideComponent implements OnInit, OnDestroy {
  @Input() dropdownPosition = "bottom-left";
  public isCollapsed: [] = [];
  @Input() mapSection: string;
  @Input() buttonLabel: string;
  @Input() saveGlobal = false;
  public buttonModal = "";

  @ContentChild("modal", { static: false }) modal: Component;
  dropdown = false;
  connexionUrl: string;
  paramsModal: NgbModalOptions;
  buttonIconClass: string;
  buttonId: string;

  constructor(
    public modalService: NgbModal,
    public coreService: VisualiseurCoreService,
    public mapService: MapService,
    public mapIdService: MapIdService
  ) {}

  ngOnInit() {}

  ngOnDestroy() {}

  @HostListener("click", ["$event"])
  click(event: MouseEvent): void {
    if (
      this.modal &&
      !this.dropdown &&
      !["measureButton"].includes(this.buttonId)
    ) {
      //this.mapService.removeInformativeDisplayLayer();
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "lg",
        centered: true,
        windowClass: "window-xl",
        container: "#modals",
      });
      if (modalRef.componentInstance.setMapId) {
        modalRef.componentInstance.setMapId(
          this.mapIdService.getId() || "visualiseur"
        );
      }
      if (modalRef.componentInstance.setSaveGlobal) {
        modalRef.componentInstance.setSaveGlobal(this.saveGlobal);
      }
    } else if (["measureButton"].includes(this.buttonId)) {
      //this.mapService.removeInformativeDisplayLayer();
      this.coreService.setMapTool(this.buttonLabel);
      this.coreService.setAnnotatingSnapping(false);
      this.modalService.dismissAll();
      const modalRef = this.modalService.open(this.modal.constructor, {
        size: "sm",
        windowClass: "window-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });
      modalRef.result.then(
        (close) => {
          this.coreService.setMapTool("");
        },
        (dismiss) => {}
      );
    }
  }

  isDropdown(): boolean {
    return this.dropdown;
  }

  setButtonModal(buttonModal: string) {
    this.buttonModal = buttonModal;
  }
}
