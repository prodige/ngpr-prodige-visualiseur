import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingButtonOverrideComponent } from './drawing-button-override.component';

describe('DrawingButtonOverrideComponent', () => {
  let component: DrawingButtonOverrideComponent;
  let fixture: ComponentFixture<DrawingButtonOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingButtonOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingButtonOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
