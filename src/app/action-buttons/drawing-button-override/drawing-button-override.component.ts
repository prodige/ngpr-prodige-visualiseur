import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionButtonsOverrideComponent } from '../action-buttons-override.component';
import { VisualiseurCoreService, MapService, MapIdService, EditingToolService } from '@alkante/visualiseur-core';

@Component({
  selector: 'alk-drawing-button-override',
  templateUrl: '../action-buttons-override.component.html',
  styleUrls: [
    '../action-buttons-override.component.scss',
    './drawing-button-override.component.scss'
  ]
})
export class DrawingButtonOverrideComponent extends ActionButtonsOverrideComponent implements OnInit {

  public isCollapsed: [] = [];
  buttonId = 'editButton';
  // buttonLabel = 'editer';
  buttonIconClass = 'fa fars-editer';
  @Input() saveGlobal = false;

  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() { }

}
