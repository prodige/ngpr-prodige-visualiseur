import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportFoncierButtonComponent } from './export-foncier-button.component';

describe('ExportFoncierButtonComponent', () => {
  let component: ExportFoncierButtonComponent;
  let fixture: ComponentFixture<ExportFoncierButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportFoncierButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportFoncierButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
