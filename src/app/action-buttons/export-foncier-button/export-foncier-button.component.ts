import { Component, OnInit } from '@angular/core';
import { VisualiseurCoreService, MapService, MapIdService } from '@alkante/visualiseur-core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionButtonsOverrideComponent } from '../action-buttons-override.component';

@Component({
  selector: 'alk-export-foncier-button',
  templateUrl: '../action-buttons-override.component.html',
  styleUrls: [
    '../action-buttons-override.component.scss',
    './export-foncier-button.component.scss'
  ]
})
export class ExportFoncierButtonComponent extends ActionButtonsOverrideComponent implements OnInit {

  public isCollapsed: [] = [];
  buttonId = 'SharingButton';
  // buttonLabel = 'partager';
  buttonIconClass = 'fa fars-partager';
  
  constructor(
    modalService: NgbModal,
    coreService: VisualiseurCoreService,
    mapService: MapService,
    mapIdService: MapIdService
  ) {
    super(modalService, coreService, mapService, mapIdService);
    super.setButtonModal('#export-foncier-modal');
    this.isCollapsed[this.buttonId] = true;
  }

  ngOnInit() { }

}
