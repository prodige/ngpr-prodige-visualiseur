import { BrowserModule } from "@angular/platform-browser";
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, registerLocaleData } from "@angular/common";
import { TreeviewModule } from "ngx-treeview";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";

import localeFr from "@angular/common/locales/fr";
import localeFrExtra from "@angular/common/locales/extra/fr";

// the second parameter 'fr' is optional
registerLocaleData(localeFr, "fr-FR", localeFrExtra);

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BootstrapComponent } from "./bootstrap/bootstrap.component";
import { HttpClientModule } from "@angular/common/http";

import { DragulaModule } from "ng2-dragula";
import {
  VisualiseurCoreModule,
  NavToolsService, MapIdService, MapModule,
} from '@alkante/visualiseur-core';

// Jquery
import { NotFoundComponent } from "./not-found/not-found.component";
import { UnauthorizedComponent } from "./unauthorized/unauthorized.component";
import { UnauthorizedDisconnectedComponent } from "./unauthorized-disconnected/unauthorized-disconnected.component";
import { EnvServiceProvider } from "./env.service.provider";
import { ExtractorToolsComponent } from "./map/tools/extractor-tools/extractor-tools.component";
import { ExtractorModalComponent } from "./map/tools/extractor-tools/extractor-modal/extractor-modal.component";
import { ExtractorButtonComponent } from "./map/tools/extractor-tools/extractor-button/extractor-button.component";
import { ActionButtonsOverrideComponent } from "./action-buttons/action-buttons-override.component";
import { ExportFoncierButtonComponent } from "./action-buttons/export-foncier-button/export-foncier-button.component";
import { DrawingButtonOverrideComponent } from "./action-buttons/drawing-button-override/drawing-button-override.component";
import { ExportFoncierModalComponent } from "./modals/export-foncier-modal/export-foncier-modal.component";
import { AddLayerModalOverrideComponent } from "./modals/add-layer-modal-override/add-layer-modal-override.component";
import { ChoseServerOverrideComponent } from "./modals/add-layer-modal-override/chose-server-override/chose-server-override.component";
import { SidebarOverrideComponent } from "./layers-legend/sidebars/sidebar-override/sidebar-override.component";
import { ChoseCatalogueResultsComponent } from "./modals/add-layer-modal-override/chose-server-override/chose-catalogue-results/chose-catalogue-results.component";
import { ResearchOverrideComponent } from "./map-tools/research-override/research-override.component";

@NgModule({
  declarations: [
    AppComponent,
    BootstrapComponent,
    NotFoundComponent,
    UnauthorizedComponent,
    UnauthorizedDisconnectedComponent,
    ExtractorToolsComponent,
    ExtractorModalComponent,
    ExtractorButtonComponent,
    ExportFoncierButtonComponent,
    ExportFoncierModalComponent,
    ActionButtonsOverrideComponent,
    DrawingButtonOverrideComponent,
    AddLayerModalOverrideComponent,
    ChoseServerOverrideComponent,
    SidebarOverrideComponent,
    ChoseCatalogueResultsComponent,
    ResearchOverrideComponent,
  ],
  entryComponents: [
    ExtractorModalComponent,
    ExportFoncierModalComponent,
    AddLayerModalOverrideComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    VisualiseurCoreModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    DragulaModule.forRoot(),
    FormsModule,
    CommonModule,
    TreeviewModule.forRoot(),
    NgbModule,
    NgSelectModule,
    MapModule
  ],
  exports: [
    ExtractorToolsComponent,
    ExtractorModalComponent,
    ExtractorButtonComponent,
    AddLayerModalOverrideComponent,
    ChoseServerOverrideComponent,
    SidebarOverrideComponent,
    ResearchOverrideComponent,
  ],
  providers: [NavToolsService, EnvServiceProvider],
  bootstrap: [BootstrapComponent],
})
export class AppModule {}
