import { Component, OnInit } from "@angular/core";
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  RouterEvent,
} from "@angular/router";

@Component({
  selector: "alk-bootstrap",
  templateUrl: "./bootstrap.component.html",
  styleUrls: ["./bootstrap.component.scss"],
})
export class BootstrapComponent implements OnInit {
  public isContextLoading = true;

  constructor(private router: Router) {
    this.router.events.subscribe((e: RouterEvent) => {
      this.navigationInterceptor(e);
    });
  }

  ngOnInit() {}

  navigationInterceptor(event: RouterEvent) {
    if (event instanceof NavigationStart) {
      this.isContextLoading = true;
    }
    if (event instanceof NavigationEnd) {
      this.isContextLoading = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.isContextLoading = false;
    }
    if (event instanceof NavigationError) {
      this.isContextLoading = false;
    }
  }
}
