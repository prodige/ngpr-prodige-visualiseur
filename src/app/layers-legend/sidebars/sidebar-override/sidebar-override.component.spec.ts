import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarOverrideComponent } from './sidebar-override.component';

describe('SidebarOverrideComponent', () => {
  let component: SidebarOverrideComponent;
  let fixture: ComponentFixture<SidebarOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
