import { Component, OnInit, Input, Host, Optional, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

import Map from 'ol/Map';
import LayerGroup from 'ol/layer/Group';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';
import { MapService, VisualiseurCoreService, MapIdService } from '@alkante/visualiseur-core';

@Component({
  selector: 'alk-sidebar-override',
  templateUrl: './sidebar-override.component.html',
  styleUrls: ['./sidebar-override.component.scss']
})
export class SidebarOverrideComponent implements OnInit, OnDestroy {

  @Input() newRootLayerGroupEE: EventEmitter<LayerGroup>;
  map: Map;
  mapId: string;

  resolutionChange = false;
  isLegendVisible = this.coreService.isApplicationInLiteMode;

  public isCollapsed = this.coreService.isApplicationInLiteMode ? false : true;

  public screenModeSubscription: Subscription = new Subscription();
  public screenMode: 'mobile' | 'desktop';
  public sideBarLegendModeSubscription: Subscription = new Subscription();
  public mode: 'Couches' | 'Légende' | 'Information' | 'Tools' | 'Open' | 'Share';

  public coreServiceSubscription: Subscription = new Subscription();
  public rootLayerGroup: LayerGroup;

  public featureToolsSubscription: Subscription = new Subscription();
  public tools: FeatureCollection['properties']['extension']['Tools'];

  constructor(
    public mapService: MapService,
    public coreService: VisualiseurCoreService,
    @Host()
    @Optional()
    public mapIdService: MapIdService
  ) { }

  ngOnInit() {
    
    this.mapId = this.mapIdService.getId() || 'visualiseur' || 'main';
    this.mapService.getMapReady(this.mapId).subscribe(map => {
      this.map = map;
    });

    this.screenModeSubscription.add(this.coreService.getInterrogatingOption().subscribe(screenMode => {
      this.screenMode = screenMode;
      this.sideBarLegendModeSubscription.add(this.mapService.getSideBarLegendMode().subscribe(mode => {
        if (this.screenMode === 'desktop') {
          if (mode !== 'Légende' && mode !== 'Couches') {
            this.mode = 'Couches';
            this.mapService.setSideBarLegendMode(this.mode);
          } else {
            this.mode = mode;
          }
        } else {
          this.mode = mode;
        }
      }));
    }));

    this.coreServiceSubscription.add(this.coreService.getRootLayerGroup().subscribe(
      (rootLayerGroup: LayerGroup) => {
        this.rootLayerGroup = rootLayerGroup;
      }
    ));

    this.featureToolsSubscription.add(this.mapService.getFeatureTools().subscribe((newTools) => {
      this.tools = newTools;
    }));
  }

  ngOnDestroy() {
    if (this.featureToolsSubscription) {
      this.featureToolsSubscription.unsubscribe();
    }
    if (this.coreServiceSubscription) {
      this.coreServiceSubscription.unsubscribe();
    }
    if (this.sideBarLegendModeSubscription) {
      this.sideBarLegendModeSubscription.unsubscribe();
    }
    if (this.screenModeSubscription) {
      this.screenModeSubscription.unsubscribe();
    }
  }

  openLegend() {
    this.isCollapsed = !this.isCollapsed;
    if (this.mode === 'Information' || this.mode === 'Tools') {
      this.changeMode('Couches');
    }
  }

  changeMode(mode: 'Légende' | 'Couches'): void {
    this.mapService.setSideBarLegendMode(mode);
  }

  changeCollapse(mode: 'Légende' | 'Couches') {
    if (mode === this.mode) {
      this.isCollapsed = !this.isCollapsed;
    }
  }

  verifyLayerGroupVisibility(layerGroup: LayerGroup, mapId: string): [boolean, boolean, boolean, boolean] {
    return this.mapService.verifyLayerGroupVisibility(layerGroup, mapId);
  }

}
