import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchOverrideComponent } from './research-override.component';

describe('ResearchOverrideComponent', () => {
  let component: ResearchOverrideComponent;
  let fixture: ComponentFixture<ResearchOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
