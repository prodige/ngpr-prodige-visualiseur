import { Subscription, Observable, forkJoin, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Component, OnInit, Optional, OnDestroy, Host } from "@angular/core";

import Map from "ol/Map";
import { HttpClient } from "@angular/common/http";
import { Point, Polygon } from "ol/geom";
import Feature from "ol/Feature";
import { get, transform } from "ol/proj";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";
import { FeatureCollection } from "@alkante/visualiseur-core/lib/models/context";
import {
  MapService,
  MapIdService,
  VisualiseurCoreService,
  EnvCoreService,
} from "@alkante/visualiseur-core";
import WKT from "ol/format/WKT";
import { BufferOp } from "jsts/org/locationtech/jts/operation/buffer";
import WKTReader from "jsts/org/locationtech/jts/io/WKTReader";
import WKTParser from "jsts/org/locationtech/jts/io/WKTParser";
import GeometryFactory from "jsts/org/locationtech/jts/geom/GeometryFactory";

@Component({
  selector: "alk-research-override",
  templateUrl: "./research-override.component.html",
  styleUrls: ["./research-override.component.scss"],
})
export class ResearchOverrideComponent implements OnInit, OnDestroy {
  map: Map;
  mapId: string;

  sessionStorageSubscription: Subscription = new Subscription();
  FAVORITE_STORAGE_KEY = "favoriteZones";

  searchAdressOpenLS: string;
  forkJoin: Subscription = new Subscription();
  favoriteZones: Array<FeatureCollection["GeobookmarkConfig"]> = [];
  favoriteZonesSearchResult = [];
  addressSearch;
  addressSearchResult = [];
  presetZones: Array<FeatureCollection["LocalisationConfig"]> = [];
  presetZonesSearchResult = [];
  contextSubscription: Subscription = new Subscription();
  tools: FeatureCollection["properties"]["extension"]["Tools"];

  isResultEmpty = true;
  inputFocused = true;
  isAddressClicked = false;

  public isCollapsed = false;

  constructor(
    protected mapService: MapService,
    private environment: EnvCoreService,
    protected mapIdService: MapIdService,
    protected coreService: VisualiseurCoreService,
    protected http: HttpClient,
    protected localStorage: LocalStorageService,
    protected sessionStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.contextSubscription.add(
      this.coreService.getContext().subscribe((context) => {
        if (!context) {
          return false;
        }
        this.favoriteZones =
          context.properties.extension.Tools.GeobookmarkConfig;
        this.presetZones =
          context.properties.extension.Tools.LocalisationConfig;
        this.searchAdressOpenLS =
          context.properties.extension.Tools.SearchAdressOpenLS;
        this.tools = context.properties.extension.Tools;
      })
    );

    let retrieved = [];
    if (this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      retrieved = this.localStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    }
    if (this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY)) {
      retrieved = this.sessionStorage.retrieve(this.FAVORITE_STORAGE_KEY);
    }
    if (retrieved && retrieved.length) {
      retrieved.forEach((zone) => {
        this.favoriteZones.push(zone);
      });
    }

    this.sessionStorageSubscription = this.sessionStorage
      .observe(this.FAVORITE_STORAGE_KEY)
      .subscribe((zones) => {
        if (zones && zones.length) {
          this.favoriteZones = zones;
        } else if (zones) {
          this.favoriteZones = zones;
        } else {
          this.favoriteZones = [];
        }
      });
  }

  ngOnDestroy() {
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.sessionStorageSubscription) {
      this.sessionStorageSubscription.unsubscribe();
    }
    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }
  }

  isLocating() {
    setTimeout(() => {
      if (!this.isAddressClicked) {
        this.inputFocused = false;
      }
    }, 500);
  }

  search(event: any) {
    if (this.forkJoin) {
      this.forkJoin.unsubscribe();
    }

    if (event.target && event.target.value) {
      const searchingWord = event.target.value;

      this.favoriteZonesSearchResult = [];
      this.addressSearchResult = [];
      this.presetZonesSearchResult = [];

      const forkJoinObservables = [];
      forkJoinObservables.push(this.getAddressResult(searchingWord));
      if (this.presetZones) {
        this.presetZones.forEach((zone) => {
          /* if (zone.criteriaInSearch) { */
          forkJoinObservables.push(
            this.getPresetZoneResult(zone, searchingWord)
          );
          /* } */
        });
      }

      this.forkJoin = forkJoin(forkJoinObservables).subscribe((responses) => {
        // Favorite zones searching part
        if (
          this.favoriteZones &&
          this.tools.Geobookmark &&
          this.tools.GeobookmarkConfig
        ) {
          this.favoriteZones.forEach((zone) => {
            if (
              zone.title
                .toLocaleLowerCase()
                .includes(searchingWord.toLowerCase())
            ) {
              const position = zone.title
                .toLowerCase()
                .indexOf(searchingWord.toLowerCase());
              const cutWord = zone.title.substring(position);

              const startVal = zone.title.substring(0, position);
              const strongVal = cutWord.substring(0, searchingWord.length);
              const endVal = cutWord.substring(0 + searchingWord.length);

              this.favoriteZonesSearchResult.push({
                start: startVal,
                strong: strongVal,
                end: endVal,
                bbox: zone.bbox,
              });
            }
          });
        }
        responses.forEach((response: any, index) => {
          // Address searching part
          if (index === 0 && response) {
            const kinds = [];
            response.results.forEach((address) => {
              if (address.poiType) {
                if (
                  kinds.filter((kind) =>
                    this.compareArrays(kind, address.poiType)
                  ).length === 0
                ) {
                  kinds.push(address.poiType);
                }
              } else if (!kinds.includes(address.kind)) {
                kinds.push(address.kind);
              }
            });
            kinds.forEach((kind) => {
              const addressSearchByKind = [];
              response.results.forEach((address) => {
                if (
                  address.kind === kind ||
                  this.compareArrays(kind, address.poiType)
                ) {
                  const position = address.fulltext
                    .toLowerCase()
                    .indexOf(searchingWord.toLowerCase());
                  const cutWord = address.fulltext.substring(position);

                  const startVal = address.fulltext.substring(0, position);
                  const strongVal = cutWord.substring(0, searchingWord.length);
                  const endVal = cutWord.substring(0 + searchingWord.length);

                  addressSearchByKind.push({
                    start: startVal,
                    strong: strongVal,
                    end: endVal,
                    coordinates: [address.x, address.y],
                  });
                }
              });
              if (addressSearchByKind.length) {
                this.addressSearchResult.push({
                  header: this.formatKind(kind),
                  links: addressSearchByKind,
                });
              }
            });
            // Preset zones searching part
          } else if (index !== 0 && response && this.tools.Localisation) {
            const presetZonesFound = response.result as FeatureCollection;
            const presetZonesResult = [];
            let presetZoneHeader = null;
            this.presetZones.forEach((zone) => {
              presetZonesFound.features.forEach((presetZones) => {
                if (presetZones.properties[zone.layerTextField]) {
                  presetZoneHeader = zone;
                  if (
                    presetZones.properties[zone.layerTextField]
                      .toLocaleLowerCase()
                      .includes(searchingWord.toLowerCase())
                  ) {
                    const position = presetZones.properties[zone.layerTextField]
                      .toLowerCase()
                      .indexOf(searchingWord.toLowerCase());
                    const cutWord =
                      presetZones.properties[zone.layerTextField].substring(
                        position
                      );

                    const startVal = presetZones.properties[
                      zone.layerTextField
                    ].substring(0, position);
                    const strongVal = cutWord.substring(
                      0,
                      searchingWord.length
                    );
                    const endVal = cutWord.substring(0 + searchingWord.length);

                    presetZonesResult.push({
                      start: startVal,
                      strong: strongVal,
                      end: endVal,
                      polygon: {
                        projection: presetZonesFound.crs.properties.name,
                        coordinates: presetZones.geometry.coordinates,
                      },
                    });
                  }
                }
              });
            });
            if (presetZonesResult.length) {
              this.presetZonesSearchResult.push({
                header: presetZoneHeader.title,
                links: presetZonesResult,
                locator: response.locator,
              });
            }
          }
        });
      });

      if (
        searchingWord !== "" ||
        this.addressSearchResult.length !== 0 ||
        this.favoriteZonesSearchResult.length !== 0 ||
        this.presetZonesSearchResult.length !== 0
      ) {
        this.isResultEmpty = false;
      } else {
        this.isResultEmpty = true;
      }
    } else {
      this.isResultEmpty = true;
    }
  }

  compareArrays(arr1, arr2) {
    if (Array.isArray(arr1) && Array.isArray(arr2))
      return arr1.length === arr2.length && arr1.every((v, i) => arr2[i] === v);
    else return false;
  }

  formatKind(kind) {
    let newKind = kind;

    switch (true) {
      case kind.includes("région"):
        newKind = "Région";
        break;
      case kind.includes("epci"):
        newKind = "EPCI";
        break;
      case kind.includes("département"):
        newKind = "Département";
        break;
      case kind.includes("préfecture"):
        newKind =
          "Préfecture" + kind.includes("préfecture de région")
            ? " de région"
            : "";
        break;
      case kind.includes("commune"):
        newKind = "Commune";
        break;
      case kind.includes("lieu-dit"):
        newKind = "Lieu-dit";
        break;
      default:
        newKind = "Adresses";
        break;
    }

    return newKind;
  }

  fitToZone(bbox: []) {
    this.isAddressClicked = true;
    this.map.getView().fit(bbox);
    this.mapService.setMapFitEvent(true);
    this.isAddressClicked = false;
  }

  locatePresetZoneOnMap(
    polygon: any,
    location = null,
    locator = null,
    searchingWord = null
  ) {
    this.isAddressClicked = true;
    let feature;
    if (polygon && polygon.projection) {
      const oldProj =
        polygon.projection.split("::")[0].split(":")[4] +
        ":" +
        polygon.projection.split("::")[1];
      feature = new Feature({
        geometry: new Polygon(polygon.coordinates),
      });

      feature
        .getGeometry()
        .transform(oldProj, this.map.getView().getProjection().getCode());

      this.map.getView().fit(feature.getGeometry().getExtent());
      this.mapService.setMapFitEvent(true);
    } else {
      feature = new Feature({
        geometry: new Point(polygon),
      });
      feature
        .getGeometry()
        .transform("EPSG:4326", this.map.getView().getProjection().getCode());
      const bufferedFeature = new Feature(feature);
      const format = new WKT();
      const wktGeom = format.writeFeature(feature);

      const wktReader = new WKTReader(new GeometryFactory());
      const jstsGeom = wktReader.read(wktGeom);

      let bufferRadius = 200;
      console.log(location);
      switch (location) {
        case "Région":
          bufferRadius = 100000;
          break;
        case "EPCI":
          bufferRadius = 65000;
          break;
        case "Département":
          bufferRadius = 60000;
          break;
        case "Préfecture":
        case "Préfecture de région":
          bufferRadius = 10000;
          break;
        case "Commune":
          bufferRadius = 4000;
          break;
        case "Lieu-dit":
          bufferRadius = 300;
          break;
        default:
          bufferRadius = 200;
          break;
      }

      const buffered = BufferOp.bufferOp(jstsGeom, bufferRadius);
      const writer = new WKTParser(new GeometryFactory());
      bufferedFeature.setGeometry(format.readGeometry(writer.write(buffered)));

      this.map.getView().fit(bufferedFeature.getGeometry().getExtent());
      this.mapService.setMapFitEvent(true);
    }

    if (locator) {
      let locateName = "";
      if (locator.layerWFSUrl.includes("typename")) {
        locateName = locator.layerWFSUrl.split("typename=")[1].split("&")[0];
      }

      this.http
        .get(
          `${this.environment.serverUrl}core/locator/queryLocator?
        account=${
          ["fr", "en"].includes(window.location.pathname.split("/")[1])
            ? window.location.pathname.split("/")[2]
            : window.location.pathname.split("/")[1]
        }
        &map=${
          ["fr", "en"].includes(window.location.pathname.split("/")[1])
            ? window.location.pathname.split("/")[3]
            : window.location.pathname.split("/")[2]
        }
        &locator=${locator.layerId}
        &locateName=${locateName}
        &optionName=${locator.layerTextField}
        &optionValue=${searchingWord}`
        )
        .subscribe((response) => {});
    }

    this.inputFocused = false;
    this.isAddressClicked = false;
  }

  getAddressResult(searchingWord: string): Observable<[]> {
    const parameters = `?gp-access-lib=2.1.2&maximumResponses=5&terr=&text=${searchingWord}&type=PositionOfInterest,StreetAddress`;
    // tslint:disable-next-line:max-line-length
    const url = this.searchAdressOpenLS + parameters;
    return this.http.get<[]>(url).pipe(
      map((result) => {
        return result;
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }

  getPresetZoneResult(zone: FeatureCollection, searchingWord): Observable<any> {
    const queries = zone.layerWFSUrl.split("?")[1].split("&");
    let body = `<?xml version="1.0" encoding="UTF-8"?>
      <GetFeature service="WFS" version="2.0.0" outputformat="geojson"
        xmlns="http://www.opengis.net/wfs/2.0"
        xmlns:cw="http://www.someserver.com/cw"
        xmlns:fes="http://www.opengis.net/ogc/1.1"
        xmlns:gml="http://www.opengis.net/gml/3.2"
        maxFeatures="5">`;
    queries.forEach((query) => {
      const name = query.split("=")[0];
      const value = query.split("=")[1];
      /* if (zone.criteriaFieldRelated) { */
      body += `
      <Query ${name}="${value}" styles="default">
        <Filter>
        <PropertyIsLike matchCase="false" wildcard="*" singleChar="." escape="!">
            <PropertyName>${zone.layerTextField}</PropertyName>
            <Literal>*${searchingWord.toUpperCase()}*</Literal>
          </PropertyIsLike>
        </Filter>
      </Query>`;
      /* } else {
        body += `<Query ${ name }="${ value }" ></Query>`;
      } */
    });
    body += `</GetFeature> `;
    return this.http.post<FeatureCollection>(zone.layerWFSUrl, body).pipe(
      map((result) => {
        return { result: result, locator: zone };
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }
}
