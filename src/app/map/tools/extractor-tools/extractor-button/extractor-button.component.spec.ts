import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtractorButtonComponent } from './extractor-button.component';

describe('ExtractorButtonComponent', () => {
  let component: ExtractorButtonComponent;
  let fixture: ComponentFixture<ExtractorButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtractorButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtractorButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
