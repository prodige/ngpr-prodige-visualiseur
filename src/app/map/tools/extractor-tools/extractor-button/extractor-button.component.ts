import { Component, OnInit, ElementRef, Input } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import {
  MapService,
  VisualiseurCoreService,
  MapIdService,
} from "@alkante/visualiseur-core";
import { ExtractorModalComponent } from "../extractor-modal/extractor-modal.component";
import Map from "ol/Map";

@Component({
  selector: "alk-buttons-extractor-button",
  templateUrl: "./extractor-button.component.html",
  styleUrls: ["./extractor-button.component.scss"],
})
export class ExtractorButtonComponent implements OnInit {
  map: Map;

  @Input() mapSection: string;
  public mapId: string;

  isExtractingEnabled = false;
  extractorTool: NgbModalRef;

  constructor(
    private modalService: NgbModal,
    private mapIdService: MapIdService,
    private mapService: MapService,
    private coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.coreService.getMapTool().subscribe((tool) => {
      if (tool === "extract") {
        this.isExtractingEnabled = true;
      } else {
        this.isExtractingEnabled = false;
        if (this.extractorTool) {
          this.extractorTool.close();
        }
      }
    });
  }

  extract() {
    this.isExtractingEnabled = !this.isExtractingEnabled;
    this.modalService.dismissAll();

    if (this.isExtractingEnabled === true) {
      this.coreService.setMapTool("extract");
      this.extractorTool = this.modalService.open(ExtractorModalComponent, {
        size: "sm",
        windowClass: "window-tool invisible-tool",
        container: "#map-container",
        backdropClass: "backdrop-tool",
        keyboard: false,
      });
    } else {
      this.coreService.setMapTool("");
    }
  }
}
