import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtractorModalComponent } from './extractor-modal.component';

describe('ExtractorModalComponent', () => {
  let component: ExtractorModalComponent;
  let fixture: ComponentFixture<ExtractorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtractorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtractorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
