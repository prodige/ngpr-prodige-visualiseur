import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { VisualiseurCoreService } from '@alkante/visualiseur-core';

@Component({
  selector: 'alk-extractor-modal',
  templateUrl: './extractor-modal.component.html',
  styleUrls: ['./extractor-modal.component.scss']
})
export class ExtractorModalComponent implements OnInit, OnDestroy {

  extractingOptionSubscription: Subscription = new Subscription;
  extractingOption: string;
  type = 'rect';
  @Input() screenMode: 'desktop' | 'mobile' = 'desktop';

  constructor(
    public activeModal: NgbActiveModal,
    private coreService: VisualiseurCoreService) { }

  ngOnInit() {
    this.coreService.setExtractingOption(this.type);
    this.extractingOptionSubscription.add(this.coreService.getExtractingOption().subscribe(option => {
      this.extractingOption = option;
    }, error => console.error(error)));
  }

  ngOnDestroy(): void {
    if (this.extractingOptionSubscription) {
      this.extractingOptionSubscription.unsubscribe();
    }
  }

  setType(typeSelected: string) {
    this.coreService.setExtractingOption(typeSelected);
  }

  setScreenMode(mode: 'desktop' | 'mobile') {
    this.screenMode = mode;
  }

}
