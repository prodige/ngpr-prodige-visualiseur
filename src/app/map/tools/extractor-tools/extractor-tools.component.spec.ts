import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtractorToolsComponent } from './extractor-tools.component';

describe('ExtractorToolsComponent', () => {
  let component: ExtractorToolsComponent;
  let fixture: ComponentFixture<ExtractorToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtractorToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtractorToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
