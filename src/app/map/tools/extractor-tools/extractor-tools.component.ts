import {
  Component,
  OnInit,
  Host,
  Optional,
  OnDestroy,
  Input,
  ViewChild,
} from "@angular/core";
import { Subscription } from "rxjs";

import Map from "ol/Map";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import { Draw } from "ol/interaction.js";
import { createBox } from "ol/interaction/Draw";
import {
  MapService,
  MapIdService,
  VisualiseurCoreService,
  DrawLayerService,
  EnvCoreService,
  UserConnectionService,
  AlertConfirmPromptModalComponent,
} from "@alkante/visualiseur-core";
import Feature from "ol/Feature";
import { WKT } from "ol/format";
import WKTReader from "jsts/org/locationtech/jts/io/WKTReader";
import WKTWriter from "jsts/org/locationtech/jts/io/WKTWriter";
import { HttpClient } from "@angular/common/http";
import Geometry from "ol/geom/Geometry";

@Component({
  selector: "alk-extractor-tools",
  templateUrl: "./extractor-tools.component.html",
  styleUrls: ["./extractor-tools.component.scss"],
})
export class ExtractorToolsComponent implements OnInit, OnDestroy {
  @Input() mapId: string;
  map: Map;
  mapToolSubscription: Subscription = new Subscription();
  extractingOptionSubscription: Subscription = new Subscription();
  drawLayerSubscription: Subscription = new Subscription();
  drawLayer: VectorLayer<VectorSource<Geometry>>;
  typeSelect: string;
  draw: Draw;
  tempDrawnFeature: Feature<Geometry>;

  catalogueUrl: string;
  format = new WKT();

  @ViewChild(AlertConfirmPromptModalComponent, { static: false })
  confirmModal: AlertConfirmPromptModalComponent;

  constructor(
    private drawLayerService: DrawLayerService,
    private mapService: MapService,
    @Host()
    @Optional()
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private http: HttpClient,
    private userConnectionService: UserConnectionService
  ) {
    this.userConnectionService.getUserConfig().subscribe((config) => {
      if (config.userInternet !== null) {
        this.catalogueUrl = config.URLS.catalogue;
      }
    });
  }

  ngOnInit() {
    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.extractingOptionSubscription.add(
      this.coreService.getExtractingOption().subscribe((opt) => {
        this.drawLayerService.generateNewDrawLayer(this.mapId);

        this.drawLayerSubscription.add(
          this.drawLayerService.getDrawLayer().subscribe(
            (vectorLayer: VectorLayer<VectorSource<Geometry>>) => {
              if (vectorLayer) {
                this.drawLayer = vectorLayer;
                this.drawLayer.setMap(this.map);

                if (this.typeSelect !== opt) {
                  this.typeSelect = opt;
                  if (this.draw) {
                    this.map.removeInteraction(this.draw);
                    this.draw.setActive(false);
                  }

                  if (this.typeSelect === "rect") {
                    this.draw = new Draw({
                      source: this.drawLayer.getSource(),
                      type: "Circle",
                      geometryFunction: createBox(),
                    });
                  } else {
                    this.draw = new Draw({
                      source: this.drawLayer.getSource(),
                      type: "Polygon",
                    });
                  }
                  this.draw.setActive(true);
                  if (this.map) {
                    this.map.addInteraction(this.draw);
                  }

                  this.draw.on("drawstart", (event) => {
                    if (this.tempDrawnFeature) {
                      this.drawLayer
                        .getSource()
                        .removeFeature(this.tempDrawnFeature);
                    }
                  });
                  this.draw.on("drawend", (event) => {
                    this.tempDrawnFeature = event.feature;
                    this.confirmModal.openModal().then((result) => {
                      if (result) {
                        window.location.href = `${
                          this.catalogueUrl
                        }/geosource/bridge?callback=handleResponseCartographicArea&param=${this.format.writeFeature(
                          this.tempDrawnFeature
                        )}`;
                      }
                    });
                  });
                }
              }
            },
            (error) => console.error(error)
          )
        );
      })
    );
  }

  ngOnDestroy() {
    if (this.extractingOptionSubscription) {
      this.extractingOptionSubscription.unsubscribe();
    }
    if (this.mapToolSubscription) {
      this.mapToolSubscription.unsubscribe();
    }
    this.map.removeInteraction(this.draw);
    this.draw.setActive(false);
  }
}
