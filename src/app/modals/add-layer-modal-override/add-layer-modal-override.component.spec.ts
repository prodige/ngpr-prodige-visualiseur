import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLayerModalOverrideComponent } from './add-layer-modal-override.component';

describe('AddLayerModalOverrideComponent', () => {
  let component: AddLayerModalOverrideComponent;
  let fixture: ComponentFixture<AddLayerModalOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLayerModalOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLayerModalOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
