import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Host,
  Optional,
  Injectable,
  OnDestroy,
} from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import Map from "ol/Map";
import LayerGroup from "ol/layer/Group";
import BaseLayer from "ol/layer/Base";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import { Subscription } from "rxjs";
import * as xml2js from "xml2js";
import { Geometry } from "ol/geom";
import VectorSource from "ol/source/Vector";
import {
  EnvCoreService,
  MapService,
  VisualiseurCoreService,
} from "@alkante/visualiseur-core";

/* declare var Jsonix: any;
declare var sourceIsEvt: any;
declare var p: any;
declare var XLink_1_0: any;
declare var OWS_1_1_0: any;
declare var SMIL_2_0_Language: any;
declare var SMIL_2_0: any;
declare var GML_3_1_1: any;
declare var OWS_1_0_0: any;
declare var OWS_1_1_0: any;
declare var Filter_1_1_0: any;
declare var Filter_2_0: any;
declare var WFS_1_1_0: any;
declare var WFS_2_0: any; */

export interface BaseLayerConfig {
  id: string;
  class: string; // 'ImageLayer' | 'TileLayer';;
  options: {
    opacity: number;
    visible: boolean;
    source: {
      [param: string]: any;
    };
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}

export interface FileReturned {
  columns: {};
  filename: string;
  uniqueId: string;
  urlWfs: string | null;
}

export interface FileWfsReturned {
  data: {
    columns: {};
    filename: string;
    uniqueId: string;
    urlWfs: string | null;
  };
  message: string;
  success: boolean;
}

@Injectable()
@Component({
  selector: "alk-add-layer-modal-override",
  templateUrl: "./add-layer-modal-override.component.html",
  styleUrls: ["./add-layer-modal-override.component.scss"],
})
export class AddLayerModalOverrideComponent implements OnInit, OnDestroy {
  mapId;
  map: Map;

  layerIdx;
  nextLayerIdxSubscription: Subscription;
  rootLayers: LayerGroup;
  layersRootSubscription: Subscription;

  labelImportText: string;
  loadingFailed = false;
  formImport: FormGroup;
  filesToUpload = new FormData();
  acceptedFileTypes =
    ".shp, .mif, .mid, .tab, .dat, .id, .map, .geojson, .kml, .csv, .shx, .dbf, .prj, .ods, .xls";

  csvColumns = [];
  projectionsList: Array<{
    key: string;
    label: string;
    proj: string;
    bounds: [number, number, number, number];
  }> = [];
  postedFile: FileReturned | FileWfsReturned;
  selectionX: any;
  selectionY: any;
  selectionProj: any;
  isFormValid = false;

  MapProjectionSubscription: Subscription;
  mapProjection: any;

  constructor(
    private mapService: MapService,
    private http: HttpClient,
    public activeModal: NgbActiveModal,
    private coreService: VisualiseurCoreService,
    private environment: EnvCoreService
  ) {
    this.formImport = new FormGroup({
      importFile: new FormControl("", Validators.required),
    });
  }

  ngOnInit() {
    this.selectionX = "noPresetSelected";
    this.selectionY = "noPresetSelected";
    this.selectionProj = "noPresetSelected";
    this.map = this.mapService.getMap(this.mapId /*  || this.mapIdService */);
    this.projectionsList = Object.values(this.environment.availableProjections);
    this.MapProjectionSubscription = this.coreService
      .getMapProjection()
      .subscribe(proj => {
        this.mapProjection = proj.getCode();
      })
    this.nextLayerIdxSubscription = this.coreService
      .getNextLayerIdx()
      .subscribe((id) => {
        this.layerIdx = id;
      });
    this.layersRootSubscription = this.coreService
      .getRootLayerGroup()
      .subscribe((root) => {
        this.rootLayers = root;
      });
  }

  ngOnDestroy() {
    if (this.nextLayerIdxSubscription) {
      this.nextLayerIdxSubscription.unsubscribe();
    }
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
    }
  }

  closeModal(event: any) {
    if (!event) {
      this.activeModal.close();
    }
  }

  setMapId(id) {
    this.mapId = id;
  }

  onFileChange(files: FileList) {
    this.filesToUpload = new FormData();
    
    this.labelImportText = Array.from(files)
      .map((f) => f.name)
      .join(", ");  
    
    
    Array.from(files).map((f) => {
      this.filesToUpload.append(f.name, f);
    });
  }

  loadFile(): void {
    this.loadingFailed = false;
    const mapExtent = this.map.getView().calculateExtent();
    this.http
      .post(
        `${this.environment.serverUrl}core/upload/upload?${
          ["fr", "en"].includes(window.location.pathname.split("/")[1])
            ? window.location.pathname.split("/")[2]
            : window.location.pathname.split("/")[1]
        }`,
        this.filesToUpload,
        {
          params: {
            srsMap: this.mapProjection, //this.map.getView().getProjection().getCode(),
            extentMinx: mapExtent[0].toString(),
            extentMiny: mapExtent[1].toString(),
            extentMaxx: mapExtent[2].toString(),
            extentMaxy: mapExtent[3].toString(),
          },
        }
      )
      .subscribe((response: any) => {
        if (response.success) {
          this.postedFile = response as FileReturned;
          if (
            this.postedFile.filename &&
            this.postedFile.filename.split(".")[1] === "csv"
          ) {
            // tslint:disable-next-line:forin
            for (const key in this.postedFile.columns) {
              this.csvColumns.push([key, this.postedFile.columns[key]]);
            }
          } else if (!this.postedFile.filename) {
            this.postedFile = response as FileWfsReturned;
            this.addLayer(this.postedFile);
            this.csvColumns = [];
          }
        } else {
          this.loadingFailed = true;
        }
      });
  }

  checkFormValidation() {
    if (
      this.selectionX !== "noPresetSelected" &&
      this.selectionY !== "noPresetSelected" &&
      this.selectionProj !== "noPresetSelected"
    ) {
      this.isFormValid = true;
    }
  }

  loadCsvFile() {
    this.loadingFailed = false;
    const mapExtent = this.map.getView().calculateExtent();
    this.http
      .get(`${this.environment.serverUrl}core/upload/addToServiceWfs`, {
        params: {
          uniqueId: (this.postedFile as FileReturned).uniqueId,
          filename: (this.postedFile as FileReturned).filename,
          colx: this.selectionX[0],
          coly: this.selectionY[0],
          srsLayer: this.selectionProj.key,
          srsMap: this.mapProjection, //this.map.getView().getProjection().getCode(),
          extentMinx: mapExtent[0].toString(),
          extentMiny: mapExtent[1].toString(),
          extentMaxx: mapExtent[2].toString(),
          extentMaxy: mapExtent[3].toString(),
        },
      })
      .subscribe((response: any) => {
        if (response.success) {
          this.postedFile = response as FileReturned;
          this.addLayer(this.postedFile);
        } else {
          this.loadingFailed = true;
        }
      });
  }

  addLayer(file: any) {
    let finalUrl = "";
    if (file.data) {
      finalUrl = `${
        file.data.urlWfs ? file.data.urlWfs : ""
      }?request=GetCapabilities&service=wfs&version=1.1.0`;
    } else {
      finalUrl = `${
        file.urlWfs ? file.urlWfs : ""
      }/?request=GetCapabilities&service=wfs&version=1.1.0`;
    }

    let wfsResponse = null;

    this.http.get(finalUrl, { responseType: "text" }).subscribe((response) => {
      const parser = new xml2js.Parser({
        trim: true,
        explicitArray: false,
      });
      parser.parseString(response, (err, result) => {
        wfsResponse = result;
      });
      /* const context = new Jsonix.Context([OWS_1_1_0, SMIL_2_0_Language, SMIL_2_0, GML_3_1_1, OWS_1_0_0, Filter_1_1_0,
        Filter_2_0, WFS_1_1_0, XLink_1_0, WFS_2_0],
        {
          namespacePrefixes : {
              'http://www.opengis.net/wfs': 'wfs',
              'http://www.opengis.net/ows': 'ows',
              'http://www.w3.org/1999/xlink': 'xlink',
              'http://www.w3.org/1999/xhtml': 'xhtml',
              'http://mapserver.gis.umn.edu/mapserver': 'ms'
          },
          mappingStyle : 'simplified'
        }
      ); */

      /* const unmarshaller = context.createUnmarshaller();
      wfsResponse = unmarshaller.unmarshalString(response); */

      if (wfsResponse) {
        let gmlFormatSelection;
        let optionsUrl = "";
        const operations =
          wfsResponse["wfs:WFS_Capabilities"]["ows:OperationsMetadata"][
            "ows:Operation"
          ];
        operations.forEach((op) => {
          if (op["$"].name === "GetFeature") {
            const parameter = op["ows:Parameter"];
            parameter.forEach((param) => {
              if (param["$"].name === "outputFormat") {
                let list = [];
                list = param["ows:Value"];
                if (list && list.length > 1) {
                  list = list
                    .filter((e) => {
                      return e.indexOf("gml") !== -1;
                      // tslint:disable-next-line:only-arrow-functions
                    })
                    .sort(function (e1, e2) {
                      const pattern = /^[^0-9]*([0-9\.]+)$/;
                      const res1 = e1.match(pattern);
                      const ve1 =
                        res1 != null && res1.length >= 2 ? res1[1] : "";
                      const res2 = e2.match(pattern);
                      const ve2 =
                        res2 != null && res2.length >= 2 ? res2[1] : "";
                      return Number(ve1 < ve2);
                    });
                }
                gmlFormatSelection = list ? list[0] : "";
              }
            });
            if (
              op["ows:DCP"]["ows:HTTP"]["ows:Get"] &&
              op["ows:DCP"]["ows:HTTP"]["ows:Get"]["$"]["xlink:href"]
            ) {
              optionsUrl =
                op["ows:DCP"]["ows:HTTP"]["ows:Get"]["$"]["xlink:href"];
            }
          }
        });
        const layerConfig = {
          id: Date.now().toString(),
          class: "VectorLayer",
          options: {
            opacity: 0.5,
            visible: true,
            extent: this.map.getView().getProjection().getExtent(),
            source: {
              class: "VectorSource",
              options: {
                format: "WFS",
                version: wfsResponse["wfs:WFS_Capabilities"]["$"].version,
                gmlFormat: gmlFormatSelection,
                typename: {
                  name: wfsResponse["wfs:WFS_Capabilities"].FeatureTypeList
                    .FeatureType.Name,
                  prefix: "",
                  ns: "",
                },
                srsMap: this.mapProjection, //this.map.getView().getProjection().getCode(),
                url: optionsUrl,
              },
            },
          },
          extension: {
            layerIdx: this.layerIdx,
            isLocalData: true,
            name: file.data ? file.data.filename : file.filename,
            displayClass: true,
            depth: 1,
            forceOpacityControl: true,
            LayerDownload: false,
            LayerLegendScaleDisplay: false,
            defaultbaseLayer: false,
            LayerQuery: {
              URLConfig: "",
              Fields: [],
            },
          },
        };

        const rootLayers = this.rootLayers.getLayers();
        const layer = this.coreService.generateLayersAndLayerGroups(
          layerConfig as BaseLayerConfig
        );
        rootLayers.insertAt(0, layer as BaseLayer);
        this.rootLayers.setLayers(rootLayers);
        this.coreService.setRootLayerGroup(this.rootLayers);
        this.coreService.updateRootLayerGroup();
        (layer as VectorLayer<VectorSource<Geometry>>).setMap(this.map);
      }

      this.activeModal.close();
    });
  }
}
