import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoseCatalogueResultsComponent } from './chose-catalogue-results.component';

describe('ChoseCatalogueResultsComponent', () => {
  let component: ChoseCatalogueResultsComponent;
  let fixture: ComponentFixture<ChoseCatalogueResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoseCatalogueResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoseCatalogueResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
