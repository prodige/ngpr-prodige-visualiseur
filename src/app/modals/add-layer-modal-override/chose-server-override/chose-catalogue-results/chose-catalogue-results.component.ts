import { Component, OnInit, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { HttpClient } from "@angular/common/http";
import {
  VisualiseurCoreService,
  AddLayerServersService,
  UrlProxyService,
} from "@alkante/visualiseur-core";
import Map from "ol/Map";
import { boundingExtent } from "ol/extent.js";
import LayerGroup from "ol/layer/Group";
import BaseLayer from "ol/layer/Base";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";

interface BaseLayerConfig {
  id: string;
  class: string; // 'ImageLayer' | 'TileLayer';;
  options: {
    opacity: number;
    visible: boolean;
    source: {
      [param: string]: any;
    };
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}
interface LayerGroupConfig {
  class: "LayerGroup";
  layers: Array<BaseLayerConfig | LayerGroupConfig>;
  options: {
    [param: string]: any;
  };
  extension: {
    [param: string]: any;
  };
}

@Component({
  selector: "alk-chose-catalogue-results",
  templateUrl: "./chose-catalogue-results.component.html",
  styleUrls: ["./chose-catalogue-results.component.scss"],
})
export class ChoseCatalogueResultsComponent implements OnInit {
  @Input() map: Map;

  @Input() catalogueResults;
  @Input() catalogueUrl;

  @Input() searchValue;
  @Input() selectedCatalogue;
  @Input() summaryCount = 0;

  /* Variables pour le résultat d'interrogation */
  currentLayerResults = [];
  resultPage = 1;
  resultSort: { [columnName: string]: "ASC" | "DESC" } = {};
  maxPage: number = 0;
  nbElemsByPage: number = 5;
  paginationRange: Array<number> = [];
  completePageRange: Array<number> = [];

  layerIdx;
  nextLayerIdxSubscription: Subscription = new Subscription();
  rootLayers: LayerGroup;
  layersRootSubscription: Subscription = new Subscription();
  jsonResponse: any;

  constructor(
    private http: HttpClient,
    private urlProxyService: UrlProxyService,
    private addLayerServersService: AddLayerServersService,
    private coreService: VisualiseurCoreService
  ) {}

  ngOnInit() {
    this.maxPage = Math.ceil(this.summaryCount / this.nbElemsByPage);
    this.selectLayerResults(this.catalogueResults.metadata);

    this.nextLayerIdxSubscription.add(
      this.coreService.getNextLayerIdx().subscribe((id) => {
        this.layerIdx = id;
      })
    );

    this.layersRootSubscription.add(
      this.coreService.getRootLayerGroup().subscribe((root) => {
        this.rootLayers = root;
      })
    );
  }

  selectLayerResults(layerResults): void {
    this.resultPage = 1;
    this.maxPage = Math.ceil(this.summaryCount / this.nbElemsByPage);
    this.changePageResult("1");
    this.recalculateCompletePageRange();
  }

  changePageResult(numPage: string): void {
    this.resultPage = parseInt(numPage, 10);
    this.paginationRange = this.range();

    let from =
      parseInt(numPage, 10) === 1 ? 1 : (parseInt(numPage, 10) - 1) * 5 + 1;
    let to = (parseInt(numPage, 10) - 1) * 5 + 5;
    this.http
      .get(
        `${this.catalogueUrl}/geonetwork/srv/fre/q?_content_type=json&any=${
          this.searchValue
        }&bucket=s101&facet.q=type%2Fdataset${
          this.selectedCatalogue
            ? "%26sourceCatalog%2F" + this.selectedCatalogue
            : ""
        }&fast=index&from=${from}&resultType=details&sortBy=relevance&to=${to}`
      )
      .subscribe((response: any) => {
        this.currentLayerResults = [];

        if (response.metadata && response.metadata.length) {
          for (let i = 0; i < 5; i++) {
            let metadata = response.metadata.filter(
              (meta, index) => index === i
            );
            if (metadata && metadata.length) {
              this.currentLayerResults.push(metadata[0]);
            }
          }
        } else if (response.metadata) {
          this.currentLayerResults.push(response.metadata);
        }
      });
  }

  range(): Array<number> {
    const tabRange = [];
    const start = Math.max(Math.min(this.maxPage - 6, this.resultPage - 2), 1);
    tabRange.length =
      start === this.maxPage - 6
        ? Math.min(this.maxPage, 7)
        : this.resultPage < 4
        ? Math.min(this.maxPage, 7)
        : this.resultPage < 5
        ? Math.min(this.maxPage, 6)
        : Math.min(this.maxPage, 5);
    return tabRange
      .fill(null)
      .map((_, idx) => start + idx)
      .filter((val) => val <= this.maxPage && val >= 1);
  }

  recalculateCompletePageRange(): void {
    const pageRange = [];
    pageRange.length = this.maxPage;
    this.completePageRange = pageRange.fill(null).map((_, idx) => 1 + idx);
  }

  getTruncateDesc(desc: string): string {
    return desc.substring(0, 250);
  }

  getImage(image): string {
    let splited = Array.isArray(image) ? image[0].split("|") : image.split("|");
    let res = "";
    splited.forEach((txt) => {
      if (txt.includes("http")) {
        res = txt;
      }
    });
    return res;
  }

  customAddLayer(metadata, type: string) {
    let layerConfig;
    const layerGroupId = new Date().getTime();
    if (type === "wms") {
      let linkInfos = this.searchForFormat(metadata, type)[0];
      layerConfig = {
        id: layerGroupId,
        class: "ImageLayer",
        options: {
          opacity: 0.5,
          visible: true,
          source: {
            class: "ImageWMS",
            options: {
              url: linkInfos[2],
              params: {
                LAYERS: linkInfos[0],
                FORMAT: "image/png",
                VERSION:
                  linkInfos[linkInfos.length - 1] === "1"
                    ? "1.0.0"
                    : linkInfos[linkInfos.length - 1],
              },
            },
          },
        },
        extension: {
          imageDataUrl: this.constructImageUrl(metadata, linkInfos),
          legendUrl: this.constructImageUrl(metadata, linkInfos),
          layerIdx: this.layerIdx,
          name: metadata.defaultTitle,
          layerName: linkInfos[0],
          rootUrl: linkInfos[2],
          displayClass: true,
          externalLayer: true,
          depth: 1,
          forceOpacityControl: true,
          LayerDownload: false,
          LayerLegendScaleDisplay: false,
          baseLayer: false,
          defaultbaseLayer: false,
        },
      };
    } else if (type === "wfs") {
      let linkInfos = this.searchForFormat(metadata, type)[0];
      /* let geoBox = metadata.geoBox ? metadata.geoBox.split('|') : [];
      console.log(geoBox) */
      layerConfig = {
        id: layerGroupId,
        class: "VectorLayer",
        options: {
          opacity: 0.5,
          visible: true,
          /* extent: (geoBox.length > 0) ? boundingExtent(geoBox[0] + ' ' + geoBox[2], geoBox[1] + ' ' + geoBox[3]) : [0, 0] , */ // boundingExtent([layer['ows:WGS84BoundingBox']['ows:LowerCorner'], layer['ows:WGS84BoundingBox']['ows:UpperCorner']]),
          source: {
            class: "VectorSource",
            options: {
              format: "WFS",
              version: linkInfos[linkInfos.length - 1],
              gmlFormat: "text/xml; subtype=gml/3.1.1",
              typename: {
                name: linkInfos[0],
                prefix: "",
                ns: "",
              },
              url: linkInfos[2],
            },
          },
        },
        extension: {
          layerIdx: this.layerIdx,
          name: metadata.defaultTitle,
          layerName: linkInfos[0],
          displayClass: true,
          externalLayer: true,
          rootUrl: linkInfos[2],
          depth: 1,
          forceOpacityControl: true,
          LayerDownload: false,
          LayerLegendScaleDisplay: false,
          defaultbaseLayer: false,
          LayerQuery: {
            URLConfig: "",
            Fields: [],
          },
        },
      };
    }

    if (layerConfig) {
      const rootLayers = this.rootLayers.getLayers();
      const layer = this.coreService.generateLayersAndLayerGroups(layerConfig);
      if (layer) {
        rootLayers.insertAt(0, layer as BaseLayer);
        this.rootLayers.setLayers(rootLayers);
        this.coreService.setRootLayerGroup(this.rootLayers);
        this.coreService.updateRootLayerGroup();
        (layer as VectorLayer<any>).setMap(this.map);
      }
    }
  }

  constructImageUrl(metadata, linkInfos) {
    let imageUrl = `${
      linkInfos[2]
    }REQUEST=GetLegendGraphic&SERVICE=WMS&FORMAT=image/png&LAYER=${
      linkInfos[1] ? linkInfos[1] : linkInfos[0]
    }&VERSION=1.0.0`;
    return imageUrl;
  }

  searchForFormat(metadata, format: string): any {
    let res = false;

    let links = [];
    if (
      metadata.link &&
      metadata.link.length &&
      typeof metadata.link !== "string"
    ) {
      metadata.link.forEach((link) => {
        let splitted = link.split("|");
        if (
          splitted.filter((txt) =>
            txt.includes("OGC:" + format.toLocaleUpperCase())
          ).length > 0
        ) {
          res = true;
          links.push(splitted);
        }
      });
    } else if (metadata.link) {
      let splitted = metadata.link.split("|");
      if (
        splitted.filter((txt) =>
          txt.includes("OGC:" + format.toLocaleUpperCase())
        ).length > 0
      ) {
        res = true;
        links.push(splitted);
      }
    }

    return res ? links : res;
  }
}
