import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoseServerOverrideComponent } from './chose-server-override.component';

describe('ChoseServerOverrideComponent', () => {
  let component: ChoseServerOverrideComponent;
  let fixture: ComponentFixture<ChoseServerOverrideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoseServerOverrideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoseServerOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
