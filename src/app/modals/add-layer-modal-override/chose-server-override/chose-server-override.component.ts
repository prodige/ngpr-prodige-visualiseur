import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Host,
  Optional,
  OnDestroy,
  Output,
  EventEmitter,
  ElementRef,
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";
import { WMSCapabilities } from "ol/format";
import { WMTSCapabilities } from "ol/format";
import { Subscription } from "rxjs";
import LayerGroup from "ol/layer/Group";
import BaseLayer from "ol/layer/Base";
import VectorLayer from "ol/layer/Vector";
import ImageLayer from "ol/layer/Image";
import TileLayer from "ol/layer/Tile";
import Map from "ol/Map";
import * as xml2js from "xml2js";
import { FeatureCollection } from "@alkante/visualiseur-core/lib/models/context";
import {
  UrlProxyService,
  MapService,
  MapIdService,
  VisualiseurCoreService,
  UserConnectionService,
  AddLayerServersService,
} from "@alkante/visualiseur-core";

@Component({
  selector: "alk-chose-server-override",
  templateUrl: "./chose-server-override.component.html",
  styleUrls: ["./chose-server-override.component.scss"],
})
export class ChoseServerOverrideComponent implements OnInit, OnDestroy {
  @Output() modalChange = new EventEmitter();

  map: Map;
  mapId: string;
  contextSubscription: Subscription = new Subscription();
  context: FeatureCollection;
  nextLayerIdxSubscription: Subscription = new Subscription();
  rootLayers: LayerGroup;
  layersRootSubscription: Subscription = new Subscription();
  fullItem;
  fullItemSubscription: Subscription = new Subscription();
  catalogueUrl: string;
  catalogueOgcUrl: string;
  catalogueOgcUrlSubscription: Subscription = new Subscription();
  choosenLayersUrl: string;
  isConnected = false;

  @Input() tabId = "";
  selection: any;
  servers;
  inputFocused = true;
  isCatalogueClicked = false;
  selectedServerUrl: string;

  selectedCatalogue: string;
  searchingCatalogueSubscribe: Subscription = new Subscription();
  catalogueSearchResult = [];
  catalogueSearchResultSelected: string;
  catalogueSummaryCount = 0;
  finalCatalogueResult = null;
  selectedFinalResult = null;

  errorResponseType: string = null;
  errorResponse: string = null;
  infoResponseType: string = null;
  infoResponse: string = null;
  isResearchClosed = false;
  isCatalogueMode = false;
  jsonResponse: any;

  treeView: Array<TreeviewItem>;
  config = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: true,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
  });
  formats: [];
  allowedFormats = ["image/png", "image/gif", "image/jpeg"];
  selectedFormat = "image/png";
  selectedLayers;
  layerIdx;

  @ViewChild("inputCatalogue", { static: false }) inputCatalogue: ElementRef;
  @ViewChild("inputUrl", { static: false }) inputUrl: ElementRef;

  constructor(
    private http: HttpClient,
    private urlProxyService: UrlProxyService,
    private addLayerServersService: AddLayerServersService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private coreService: VisualiseurCoreService,
    private userConnectionService: UserConnectionService
  ) {}

  ngOnInit() {
    if (this.tabId !== "catalogue") {
      this.isCatalogueMode = false;
    } else {
      this.isCatalogueMode = true;
    }

    this.selection = "noPresetSelected";

    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.mapService.getMapReady(this.mapId).subscribe((map) => {
      this.map = map;
    });

    this.selectedLayers = [];
    this.contextSubscription.add(
      this.coreService.getContext().subscribe((context) => {
        if (!context) {
          return false;
        }
        this.context = context;
      })
    );
    this.nextLayerIdxSubscription.add(
      this.coreService.getNextLayerIdx().subscribe((id) => {
        this.layerIdx = id;
      })
    );
    this.layersRootSubscription.add(
      this.coreService.getRootLayerGroup().subscribe((root) => {
        this.rootLayers = root;
      })
    );
    this.fullItemSubscription.add(
      this.addLayerServersService.getFullItem().subscribe((item) => {
        this.fullItem = item;
      })
    );
    this.catalogueOgcUrlSubscription.add(
      this.userConnectionService.getUserConfig().subscribe((user) => {
        this.isConnected =
          user.userInternet !== null ? !user.userInternet : null;
        this.catalogueUrl = user.URLS.catalogue;
        this.catalogueOgcUrl = user.URLS.admincarto;
        if (this.tabId !== "catalogue") {
          this.http
            .get(
              `${this.catalogueOgcUrl}/services/catalogue/getOGC/` +
                this.tabId.toUpperCase()
            )
            .subscribe((response) => {
              this.servers = response;
            });
        } else {
          this.http
            .get(`${this.catalogueUrl}/geonetwork/srv/api/0.1/sources`)
            .subscribe((response) => {
              this.servers = response;
            });
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.nextLayerIdxSubscription) {
      this.nextLayerIdxSubscription.unsubscribe();
    }
    if (this.layersRootSubscription) {
      this.layersRootSubscription.unsubscribe();
    }
    if (this.fullItemSubscription) {
      this.fullItemSubscription.unsubscribe();
    }
    if (this.contextSubscription) {
      this.contextSubscription.unsubscribe();
    }
    if (this.catalogueOgcUrlSubscription) {
      this.catalogueOgcUrlSubscription.unsubscribe();
    }
  }

  select(event: any) {
    console.log(event);
    if (typeof event.URL !== "undefined") {
      this.selectedServerUrl = event.URL;
    } else if (event.uuid) {
      this.selectedCatalogue = event.uuid;
      //this.inputCatalogue.nativeElement.value = event.name;
    }
  }

  search(event: any) {
    if (this.searchingCatalogueSubscribe) {
      this.searchingCatalogueSubscribe.unsubscribe();
    }

    if (event.target && event.target.value) {
      const searchingWord = event.target.value;
      this.catalogueSearchResult = [];

      this.searchingCatalogueSubscribe = this.http
        .get(
          `${this.catalogueUrl}/geonetwork/srv/fre/suggest?field=anylight&sortBy=STARTSWITHFIRST&q=${searchingWord}`
        )
        .subscribe((response) => {
          response[1].forEach((result) => {
            if (
              result.toLocaleLowerCase().includes(searchingWord.toLowerCase())
            ) {
              const position = result
                .toLowerCase()
                .indexOf(searchingWord.toLowerCase());
              const cutWord = result.substring(position);

              const startVal = result.substring(0, position);
              const strongVal = cutWord.substring(0, searchingWord.length);
              const endVal = cutWord.substring(0 + searchingWord.length);

              this.catalogueSearchResult.push({
                start: startVal,
                strong: strongVal,
                end: endVal,
              });
            }
          });
        });
    }
  }

  isLocating() {
    setTimeout(() => {
      this.inputFocused = false;
    }, 500);
  }

  setCatalogueSearchResultSelected(select) {
    this.isCatalogueClicked = true;
    this.catalogueSearchResultSelected =
      select.start + select.strong + select.end;
  }

  searchForLayers() {
    this.choosenLayersUrl = this.inputUrl
      ? this.inputUrl.nativeElement.value
      : null;
    this.treeView = null;
    this.finalCatalogueResult = null;
    this.selectedFinalResult = null;
    this.errorResponseType = null;
    this.errorResponse = null;
    this.infoResponseType = null;
    this.infoResponse = null;
    this.jsonResponse = "";
    const options = [
      ["REQUEST", "GetCapabilities"],
      ["SERVICE", this.tabId.toUpperCase()],
    ];
    if (this.tabId === "wfs") {
      options.push(["VERSION", "1.1.0"]);
    }
    if (this.tabId !== "catalogue") {
      const httpUrl = this.urlProxyService.getFinalUrl(
        this.inputUrl.nativeElement.value,
        options
      );
      this.http.get(httpUrl, { responseType: "text" }).subscribe(
        (response) => {
          let parser;
          if (this.tabId === "wms") {
            parser = new WMSCapabilities();
            try {
              this.jsonResponse = parser.read(response);
              if (this.jsonResponse && this.jsonResponse.Capability) {
                this.isResearchClosed = true;
                this.treeView = this.addLayerServersService.constructTree(
                  this.tabId,
                  [],
                  this.jsonResponse.Capability,
                  this.map.getView().getProjection().getCode()
                );
                if (!this.treeView.length) {
                  this.treeView = null;
                  this.infoResponseType = "NO_COMPATIBLE_LAYER";
                  this.infoResponse =
                    "Il n'y a pas de couche compatible avec la projection de la carte";
                  this.isResearchClosed = false;
                } else {
                  this.formats =
                    this.jsonResponse.Capability.Request.GetMap.Format.filter(
                      (f) => this.allowedFormats.includes(f)
                    );
                }
              } else {
                this.treeView = null;
                this.isResearchClosed = false;
                this.infoResponseType = "INVALID_URL";
                this.infoResponse =
                  "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
              }
            } catch (error) {
              this.errorResponseType = "INVALID_FORMAT";
              this.errorResponse = "Le format récupéré est invalide.";
              this.isResearchClosed = false;
            }
          } else if (this.tabId === "wfs") {
            // this.version = parser.read(response).version;
            // xmlDoc.documentElement.getAttribute('version').trim();
            try {
              /* const context = new Jsonix.Context([OWS_1_1_0, SMIL_2_0_Language, SMIL_2_0, GML_3_1_1, OWS_1_0_0, Filter_1_1_0,
              Filter_2_0, WFS_1_1_0, XLink_1_0, WFS_2_0],
              {
                namespacePrefixes : {
                    'http://www.opengis.net/wfs': 'wfs',
                    'http://www.opengis.net/ows': 'ows',
                    'http://www.w3.org/1999/xlink': 'xlink',
                    'http://www.w3.org/1999/xhtml': 'xhtml',
                    'http://mapserver.gis.umn.edu/mapserver': 'ms'
                },
                mappingStyle : 'simplified'
              }
            );
            // Create an unmarshaller (parser)
            const unmarshaller = context.createUnmarshaller();
            this.jsonResponse = unmarshaller.unmarshalString(response); */

              const parser = new xml2js.Parser({
                trim: true,
                explicitArray: false,
              });
              parser.parseString(response, (err, result) => {
                this.jsonResponse = result;
              });

              if (this.jsonResponse["wfs:WFS_Capabilities"]) {
                this.isResearchClosed = true;
                this.treeView = [
                  new TreeviewItem({
                    // text: this.jsonResponse['wfs:WFS_Capabilities'].serviceIdentification.title[0].value,
                    text: this.jsonResponse["wfs:WFS_Capabilities"][
                      "ows:ServiceIdentification"
                    ]["ows:Title"],
                    value: "",
                    checked: false,
                    children: this.addLayerServersService.constructTree(
                      this.tabId,
                      [],
                      this.jsonResponse["wfs:WFS_Capabilities"].FeatureTypeList
                    ),
                  }),
                ];
              } else {
                this.treeView = null;
                this.isResearchClosed = false;
                this.infoResponseType = "INVALID_URL";
                this.infoResponse =
                  "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
              }
            } catch (error) {
              this.treeView = null;
              this.isResearchClosed = false;
              this.infoResponseType = "INVALID_URL";
              this.infoResponse =
                "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
            }
          } else if (this.tabId === "wmts") {
            parser = new WMTSCapabilities();
            this.jsonResponse = parser.read(response);
            if (this.jsonResponse.Contents) {
              this.isResearchClosed = true;
              const treeViewChildren =
                this.addLayerServersService.constructTree(
                  this.tabId,
                  [],
                  this.jsonResponse.Contents,
                  this.map.getView().getProjection().getCode()
                );
              if (treeViewChildren.length) {
                this.treeView = [
                  new TreeviewItem({
                    text: this.jsonResponse.ServiceIdentification.Title
                      ? this.jsonResponse.ServiceIdentification.Title
                      : this.jsonResponse.ServiceIdentification.ServiceType,
                    value: "",
                    checked: false,
                    children: treeViewChildren,
                  }),
                ];
              } else {
                this.treeView = null;
                this.isResearchClosed = false;
                this.infoResponseType = "NO_COMPATIBLE_LAYER";
                this.infoResponse =
                  "Il n'y a pas de couche compatible avec la projection de la carte";
              }
            } else {
              this.treeView = null;
              this.isResearchClosed = false;
              this.infoResponseType = "INVALID_URL";
              this.infoResponse =
                "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
            }
          }

          const parserDom = new DOMParser();
          const xmlDoc = parserDom.parseFromString(response, "text/xml");
          if (
            xmlDoc.documentElement.getElementsByTagName("ServiceException")
              .length ||
            xmlDoc.documentElement.getElementsByTagName("ExceptionReport")
              .length
          ) {
            this.isResearchClosed = false;
            this.errorResponseType = "INVALID_URL";
            this.errorResponse =
              "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
          }
        },
        (error) => {
          if (error.status === 404) {
            this.errorResponseType = "NOT_FOUND";
            this.errorResponse =
              "Le serveur est injoignable ou ne répond pas correctement";
          } else if (error.status === 403) {
            this.errorResponseType = "UNAUTHORIZED";
            this.errorResponse = "L'accès au serveur n'est pas autorisé";
          } else {
            this.errorResponseType = "INVALID_URL";
            this.errorResponse =
              "Il semblerait que l'url que vous avez insérée soit invalide. Veuillez utiliser une autre url.";
          }
          this.isResearchClosed = false;
        }
      );
    } else {
      this.catalogueSearchResultSelected =
        this.inputCatalogue.nativeElement.value;
      this.http
        .get(
          `${this.catalogueUrl}/geonetwork/srv/fre/q?_content_type=json&any=${
            this.inputCatalogue.nativeElement.value
          }&bucket=s101&facet.q=type%2Fdataset${
            this.selectedCatalogue
              ? "%26sourceCatalog%2F" + this.selectedCatalogue
              : ""
          }&fast=index&from=1&resultType=details&sortBy=relevance&to=5`
        )
        .subscribe((response: any) => {
          this.finalCatalogueResult = response;
          this.catalogueSummaryCount = response.summary["@count"];
          if (this.finalCatalogueResult.metadata) {
            this.selectedFinalResult = this.finalCatalogueResult.metadata[0];
          }
        });
    }
  }

  addLayers() {
    let hasAddLayer = false;
    if (this.selectedLayers) {
      this.selectedLayers.forEach((selectedLayer, index) => {
        const options = [
          ["REQUEST", "GetLegendGraphic"],
          ["SERVICE", this.tabId.toUpperCase()],
          ["FORMAT", "image/png"],
          ["LAYER", selectedLayer.Name],
        ];
        if (this.tabId === "wfs") {
          options.push(["VERSION", "1.1.0"]);
        } else if (this.tabId === "wms") {
          options.push(["VERSION", "1.0.0"]);
        }
        const legendLayerUrl = this.urlProxyService.getFinalUrl(
          this.choosenLayersUrl,
          options
        );

        const layerConfig = this.addLayerServersService.addLayer(
          this.tabId,
          selectedLayer,
          index,
          this.jsonResponse,
          this.layerIdx,
          this.selectedFormat,
          this.map.getView().getProjection().getCode(),
          legendLayerUrl
        );
        if (layerConfig) {
          hasAddLayer = true;
          const rootLayers = this.rootLayers.getLayers();
          const layer =
            this.coreService.generateLayersAndLayerGroups(layerConfig);
          if (layer) {
            rootLayers.insertAt(0, layer as BaseLayer);
            this.rootLayers.setLayers(rootLayers);
            this.coreService.setRootLayerGroup(this.rootLayers);
            this.coreService.updateRootLayerGroup();
            (layer as VectorLayer<any>).setMap(this.map);
          }
        }
      });
      if (hasAddLayer) {
        this.modalChange.emit(false);
      }
    }
  }

  onSelectedChange(selectedLayers: Array<any>) {
    this.selectedLayers = [];
    if (selectedLayers) {
      if (selectedLayers.length) {
        selectedLayers.forEach((layer) => {
          if (this.tabId === "wms") {
            this.addLayerServersService.searchItem(
              this.tabId,
              layer.value,
              this.jsonResponse.Capability
            );
            this.selectedLayers.push(this.fullItem);
          } else if (this.tabId === "wfs") {
            this.addLayerServersService.searchItem(
              this.tabId,
              layer.value,
              this.jsonResponse["wfs:WFS_Capabilities"].FeatureTypeList
            );
            this.selectedLayers.push(this.fullItem);
          } else if (this.tabId === "wmts") {
            this.addLayerServersService.searchItem(
              this.tabId,
              layer.value,
              this.jsonResponse.Contents
            );
            this.selectedLayers.push(this.fullItem);
          }
        });
      }
    }
  }
}
