import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportFoncierModalComponent } from './export-foncier-modal.component';

describe('ExportFoncierModalComponent', () => {
  let component: ExportFoncierModalComponent;
  let fixture: ComponentFixture<ExportFoncierModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportFoncierModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportFoncierModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
