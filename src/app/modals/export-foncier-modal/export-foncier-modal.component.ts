import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  MapService,
  DrawLayerService,
  VisualiseurCoreService,
  EditingToolService,
  MapIdService,
  EnvCoreService,
} from "@alkante/visualiseur-core";
import { Subscription } from "rxjs";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import Collection from "ol/Collection";
import BaseLayer from "ol/layer/Base";
import { HttpClient } from "@angular/common/http";
import { FeatureCollection } from "@alkante/visualiseur-core/lib/models/context";
import { WKT } from "ol/format";
import LayerGroup from "ol/layer/Group";
import { Style, Fill, Stroke } from "ol/style";
import { getArea } from "ol/sphere";
import { getCenter } from "ol/extent";
import Map from "ol/Map";
import Polygon from "ol/geom/Polygon";

@Component({
  selector: "alk-export-foncier-modal",
  templateUrl: "./export-foncier-modal.component.html",
  styleUrls: ["./export-foncier-modal.component.scss"],
})
export class ExportFoncierModalComponent implements OnInit, OnDestroy {
  map: Map;
  mapId: string;

  exportForm = new FormGroup({
    titre: new FormControl("", Validators.required),
    identifiant: new FormControl("", Validators.required),
    nom: new FormControl("", Validators.required),
    prenom: new FormControl("", Validators.required),
    ref: new FormControl("", Validators.required),
    surface: new FormControl(0, Validators.required),
    commentaire: new FormControl(""),
    format: new FormControl("A4", Validators.required),
  });

  subscriptions: Subscription = new Subscription();

  private rootLayerGroup: LayerGroup | BaseLayer | (LayerGroup | BaseLayer)[];
  private newContext: FeatureCollection;
  private context: FeatureCollection;

  lastFeatureAdded: any = null;

  format = new WKT();

  constructor(
    private environment: EnvCoreService,
    public http: HttpClient,
    public activeModal: NgbActiveModal,
    public mapService: MapService,
    private coreService: VisualiseurCoreService,
    private editingToolService: EditingToolService,
    private mapIdService: MapIdService,
    private drawLayerService: DrawLayerService
  ) {}

  ngOnInit() {
    this.subscriptions.add(
      this.coreService.getContext().subscribe((context) => {
        this.context = context;
        this.newContext = JSON.parse(JSON.stringify(this.context));
        this.exportForm.get("titre").setValue(context.properties.title);
        this.rootLayerGroup = this.coreService.generateLayersAndLayerGroups(
          context.properties.extension.layers
        );
      })
    );

    this.mapId = this.mapIdService.getId() || "visualiseur" || "main";
    this.subscriptions.add(
      this.mapService.getMapReady(this.mapId).subscribe((map) => {
        this.map = map;

        if (this.map) {
          this.subscriptions.add(
            this.editingToolService.getGlobalFeature().subscribe((feature) => {
              this.lastFeatureAdded = feature;
              if (this.lastFeatureAdded) {
                this.exportForm
                  .get("identifiant")
                  .setValue(this.lastFeatureAdded.properties.gid);
                this.exportForm
                  .get("nom")
                  .setValue(this.lastFeatureAdded.properties.nom);
                this.exportForm
                  .get("prenom")
                  .setValue(this.lastFeatureAdded.properties.prenom);
                this.exportForm
                  .get("ref")
                  .setValue(this.lastFeatureAdded.properties.ref_cadastrale);
                this.exportForm
                  .get("commentaire")
                  .setValue(this.lastFeatureAdded.properties.commentaire);

                // Ajout d'un layer avec la feature
                let feature = this.format.readFeature(
                  this.lastFeatureAdded.geometry
                );

                const area =
                  this.map.getView().getProjection().getUnits() == "degrees"
                    ? getArea(feature.getGeometry(), {
                        projection: this.map.getView().getProjection(),
                      })
                    : (feature.getGeometry() as Polygon).getArea();
                const areaInHa = Math.round((area / 10000) * 100) / 100;

                this.exportForm.get("surface").setValue(areaInHa);

                this.drawLayerService
                  .getPolygonStyleOptions()
                  .subscribe((style: any) => {
                    style.settedStyle.color = "rgba(255, 0, 105, 0.87)";
                    style.settedStyle.background = "rgba(255, 0, 105, 0.87)";
                    feature.set("settedStyle", style.settedStyle);

                    let vectorSource = new VectorSource({
                      features: [feature],
                    });
                    vectorSource.set("class", "VectorSource");

                    let vectorLayer = new VectorLayer({
                      source: vectorSource,
                      zIndex: Infinity,
                      style: new Style({
                        fill: new Fill({
                          color: "rgba(255, 0, 105, 0.87)",
                        }),
                        stroke: new Stroke({
                          color: "rgba(255, 0, 105, 0.87)",
                          width: 3,
                        }),
                      }),
                    });

                    vectorLayer.set("name", "Donnée Ofag éditée");
                    vectorLayer.set("visibleChecked", true);
                    vectorLayer.set("class", "VectorLayer");

                    let rootLayerGroup: LayerGroup =
                      this.rootLayerSearchRecursive(
                        (this.rootLayerGroup as LayerGroup).getLayers()
                      );
                    rootLayerGroup.getLayers().insertAt(0, vectorLayer);
                    this.mapService.verifyLayerGroupVisibility(
                      rootLayerGroup,
                      this.mapId
                    );

                    this.newContext.properties.extension.layers.layers =
                      this.coreService.contextLayersAndLayerGroups(
                        rootLayerGroup.getLayers()
                      );
                  });
              }
            })
          );
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  setVisible(layerGroup) {
    if (layerGroup) {
      layerGroup.forEach((rootLayer) => {
        if (rootLayer.class === "LayerGroup") {
          rootLayer = this.setVisible(rootLayer.layers);
        } else {
          rootLayer.options.visible = true;
        }
      });
    }
  }

  rootLayerSearchRecursive(
    root: Collection<BaseLayer>,
    layerGroup: LayerGroup = null
  ): LayerGroup {
    const layersList = [];
    root.getArray().forEach((rootLayer) => {
      if (rootLayer.get("class") === "LayerGroup") {
        rootLayer = this.rootLayerSearchRecursive(
          (rootLayer as LayerGroup).getLayers(),
          rootLayer as LayerGroup
        );
        if ((rootLayer as LayerGroup).getLayers().getArray().length !== 0) {
          layersList.push(rootLayer);
        }
      } else {
        if (rootLayer.get("visible") || rootLayer.get("visibleChecked")) {
          layersList.push(rootLayer);
        }
      }
    });
    const returnGroup = new LayerGroup({
      layers: layersList,
    });

    returnGroup.set(
      "name",
      layerGroup && layerGroup.get("name") ? layerGroup.get("name") : null
    );

    return returnGroup;
  }

  selectImageFormat(format: string) {
    this.exportForm.get("format").setValue(format);
  }

  prepareExportRequest() {
    let feature = this.format.readFeature(this.lastFeatureAdded.geometry);
    let coords = (feature.getGeometry() as Polygon).getCoordinates();
    this.newContext.properties.bbox = feature.getGeometry().getExtent();

    let x =
      this.newContext.properties.bbox[0] +
      (this.newContext.properties.bbox[2] -
        this.newContext.properties.bbox[0]) /
        2;
    let y =
      this.newContext.properties.bbox[1] +
      (this.newContext.properties.bbox[3] -
        this.newContext.properties.bbox[1]) /
        2;

    this.setVisible(this.newContext.properties.extension.layers.layers);

    this.http
      .post(
        `${this.environment.serverUrl}core/print/exportOfag?account=${
          ["fr", "en"].includes(window.location.pathname.split("/")[1])
            ? window.location.pathname.split("/")[2]
            : window.location.pathname.split("/")[1]
        }`,
        {
          context: this.newContext,
          comment: this.exportForm.get("commentaire").value,
          identifiant: this.exportForm.get("identifiant").value,
          nom: this.exportForm.get("nom").value,
          prenom: this.exportForm.get("prenom").value,
          refcadast: this.exportForm.get("ref").value,
          coord: coords[0],
          surface: this.exportForm.get("surface").value
            ? this.exportForm.get("surface").value
            : 20,
          title: this.exportForm.get("titre").value,
          pagesize: this.exportForm.get("format").value,
          model: "demandefoncier.tpl",
          type: "pdf",
        },
        {
          responseType: "blob",
        }
      )
      .subscribe((responsePostImpressionRequest: Blob) => {
        const link = document.createElement("a");
        link.href = window.URL.createObjectURL(responsePostImpressionRequest);
        link.download =
          "impr_" + Math.random().toString(36).substr(2, 9) + ".pdf";
        link.dispatchEvent(
          new MouseEvent(`click`, {
            bubbles: true,
            cancelable: true,
            view: window,
          })
        );
      });
  }
}
