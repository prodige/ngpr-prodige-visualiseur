import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "alk-not-found",
  templateUrl: "./not-found.component.html",
  styleUrls: ["./not-found.component.scss"],
})
export class NotFoundComponent implements OnInit {
  currentUrl = "";

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.currentUrl = this.route.snapshot.params.message
      ? null
      : this.route.snapshot.params.account
      ? `${window.location.origin}/${this.route.snapshot.params.account}/${this.route.snapshot.params.context}`
      : null;
  }
}
