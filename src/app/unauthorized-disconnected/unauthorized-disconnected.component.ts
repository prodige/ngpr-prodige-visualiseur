import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'alk-unauthorized-disconnected',
  templateUrl: './unauthorized-disconnected.component.html',
  styleUrls: ['./unauthorized-disconnected.component.scss']
})
export class UnauthorizedDisconnectedComponent implements OnInit {

  redirectUrl: string;

  constructor() { }

  ngOnInit() {
    this.redirectUrl = history.state ? window.location.origin + history.state.oldUrl : '';
  }

}
