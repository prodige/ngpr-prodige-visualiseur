(function (window) {
  
    $ENV = window.__env = window.__env || {};
  
    /**
     * Ce fichier permet de surcharger les données d'environement du fichier env.service.
     * Décommenter uniquement ce qui doit être surchargé.
     */

    $ENV.serverUrl = 'https://carto-test.atlasante.fr/';
    // vaccins_stockage.map

    // $ENV.serverUrl = 'https://carto.geoguyane.fr/';
    // fr/1/maia.map?object=layer23;wdpaid;183431

    // $ENV.serverUrl = 'https://carto.datara.gouv.fr/';
    // r_gestion_crise_d69.map
    // dreal_nature_paysage_r82.map

    // $ENV.serverUrl = 'https://carto.picto-occitanie.fr/';
    // visu_donnees_energie2.map

    // $ENV.serverUrl = 'https://carto.sigloire.fr/';
    // n_sre_eolien_r52.map

    // $ENV.serverUrl = 'https://carto-xp-pf-dgitm.application-prodige-mig.fr/';
    // carte_test_ilo_ign.map
    // carte_interactive_irve_en_service_des_aires_du_rrn.map
  
    // $ENV.serverUrl = 'https://carto.geoguyane.fr/';
    // cartOFAG.map
  
    // $ENV.serverUrl = 'https://carto.geoguyane.fr/';
    $ENV.scaleList = [ 1, 2500, 5000, 10000, 18100, 36100, 50000, 100000, 250000, 500000, 1000000, 2000000, 4000000, 10000000 ];
    // $ENV.serverUrl = 'https://carto.prodige.internal/';
    // $ENV.serverUrl = 'https://carto-test.atlasante.fr/';

    /* window.__env.portraitRatio = 0.795;
    window.__env.landscapeRatio = 1.258; */
    // window.__env.prodige41Url = 'https://prodige41.alkante.al:38001/';
    // window.__env.frontCartoUrl = 'https://prodige41.alkante.al:38001/';
  
    // window.__env.interrogationPointPrecision = 5;
    // window.__env.interrogationTooltipPrecision = 5;
    // window.__env.defaultMousePositionProjection = 'EPSG:4326';
    /* window.__env.availableProjections = {
      4326: {
        key: 'EPSG:4326',
        label: 'WGS84',
        proj: '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs',
        bounds: [-180.0000, -90.0000, 180.0000, 90.0000]
      }, ...
    }; */
}(this));